package colors

import "runtime"

var Reset = "\033[0m"

var Black = "\033[1;90m"
var Red = "\033[1;31m"
var Green = "\033[1;32m"
var Yellow = "\033[33m"
var Blue = "\033[34m"
var Purple = "\033[1;35m"
var Cyan = "\033[1;36m"
var Gray = "\033[37m"
var White = "\033[97m"

func init() {
	if runtime.GOOS == "windows" {
		Reset = ""
		Red = ""
		Green = ""
		Yellow = ""
		Blue = ""
		Purple = ""
		Cyan = ""
		Gray = ""
		White = ""
	}
}
