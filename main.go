package main

import (
	"strings"

	"github.com/chzyer/readline"

	"tp_vigenere/colors"
	h "tp_vigenere/util"
)

func main() {

	helper := h.NewUtil()
	helper.CleanConsole()

	rl, err := readline.New("")
	if err != nil {
		panic(err)
	}
	defer rl.Close()

	//rl.SetPrompt()

	for true {
		println(
			colors.Red + "\nSelect Mode" + colors.Reset + " (number)" +
				colors.Cyan + "\n\n 1) Encryption\t2) Decryption\n" + colors.Reset)

		mode, err := rl.Readline()
		if err != nil {
			break
		}

		helper.CleanConsole()

		if strings.ToLower(mode) == "1" {
			Encryption(helper, rl)
			break

		} else if strings.ToLower(mode) == "2" {

			Decryption(helper, rl)
			break
		}
		println(colors.Red + "Enter A Valid Option\n\n" + colors.Reset)
	}

}

func Encryption(helper h.Util, rl *readline.Instance) {
	rl.SetPrompt("Texte a entre: > ")
	println(colors.Red + "Encryption Mode" + colors.Reset)

	plainText := helper.GetTextFromUser(rl, "PlainText please")
	key := helper.GetTextFromUser(rl, "Key please")

	helper.CleanConsole()
	cipherText := helper.VigenereEncryption(plainText, key)

	println(colors.Green+"PlainText & Key:", colors.Reset)
	println(colors.Yellow, "\t>>>", colors.Reset, colors.Red, plainText, " & ", key, colors.Reset)

	println()

	println(colors.Green+"CipherText:", colors.Reset)
	println(colors.Yellow, "\t>>>", colors.Reset, colors.Red, cipherText, colors.Reset)

}

func Decryption(helper h.Util, rl *readline.Instance) {

	rl.SetPrompt("Texte a entre: > ")
	println(colors.Red + "Encryption Mode" + colors.Reset)

	cipherText := helper.GetTextFromUser(rl, "CipherText please")
	key := helper.GetTextFromUser(rl, "Key please")

	helper.CleanConsole()
	plainText := helper.VigenereDecryption(cipherText, key)

	println(colors.Green+"CipherText & Key:", colors.Reset)
	println(colors.Yellow, "\t>>>", colors.Reset, colors.Red, cipherText, " & ", key, colors.Reset)

	println()

	println(colors.Green+"PlainText:", colors.Reset)
	println(colors.Yellow, "\t>>>", colors.Reset, colors.Red, plainText, colors.Reset)

}
