package util

import (
	"fmt"
	"strings"
	"tp_vigenere/colors"

	"regexp"

	"github.com/chzyer/readline"
)

var r, _ = regexp.Compile("[A-Za-z]")

type Util struct {
}

func NewUtil() Util {
	return Util{}
}

func (u Util) uppercaseAndRemoveSpaces(s string) string {
	return strings.ToUpper(strings.ReplaceAll(s, " ", ""))
}

// Encryptions Using Caeser to a char
func (u Util) caesarEncryption(pt rune, key int) rune {
	pt = pt - 65
	return ((pt + rune(key-65)) % 26) + 65
}

// Decryptions Using Caeser to a char
func (u Util) caesarDecryption(st rune, key int) rune {
	st = st - 65
	result := (st - rune(key-65)) % 26

	if result >= 0 {
		return result + 65
	} else {
		return (26 + result) + 65
	}
}

// Encryption Function
func (u Util) VigenereEncryption(plainText string, key string) string {

	var newResult string
	for i := 0; i < len(plainText); i++ {
		c := u.caesarEncryption(rune(plainText[i]), int(key[i%(len(key))]))
		newResult = newResult + string(c)
	}
	return newResult
}

// Decryption Function
func (u Util) VigenereDecryption(cipherText string, key string) string {

	var newResult string
	for i := 0; i < len(cipherText); i++ {
		c := u.caesarDecryption(rune(cipherText[i]), int(key[i%(len(key))]))
		newResult = newResult + string(c)
	}
	return newResult
}

func (u Util) GetTextFromUser(rl *readline.Instance, str string) string {

	var line string
	var upppercaseLine string
	var err error

	println("\n" + colors.Green + str + colors.Reset)

	line, err = rl.Readline()
	if err != nil { // io.EOF
		panic("string hmmm")
	}

	upppercaseLine = u.uppercaseAndRemoveSpaces(line)
	upppercaseLine = strings.Join(r.FindAllString(upppercaseLine, len(upppercaseLine)), "")

	return upppercaseLine
}

func (u Util) CleanConsole() {

	fmt.Print("\033[H\033[2J")
}
