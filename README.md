# How to Run it

## clone the repository
```
git clone https://gitlab.com/Halladj/vigenere
```

## get inside repository
```
cd vigenere
```

## install dependencies
```
go get -u github.com/Davincible/goinsta/v3@latest
```

## run the app
```
go run main
```
